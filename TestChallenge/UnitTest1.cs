using NUnit.Framework;
using Graphos;

namespace TestChallenge
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestPairs1()
        {
            int[,] pairs = {
                {10, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 17},
                {4, 5}, {4, 8}, {8, 9}, {3, 1}
            };
            GraphChallenge.PrintGraphChallenge(pairs);
        }
        [Test]
        public void TestPairs2()
        {
            int[,] pairs = {
                {10, 3}, {2, 10}, {7, 10}, {5, 6}, {5, 17},
                {4, 5}, {4, 8}, {8, 9}, {3, 1}
            };
            GraphChallenge.PrintGraphChallenge(pairs);
        }
        [Test]
        public void TestPairs3()
        {
            int[,] pairs = {
                {22, 1}, {7, 1}, {1, 16}, {23, 16}, {23, 7},
                {9, 23}, {9, 6}, {6, 4}, {1, 8}
            };
            GraphChallenge.PrintGraphChallenge(pairs);
        }
        [Test]
        public void TestCalculation1()
        {            
            CalculatorClass.CalculateOnConsole("1+2+3+4+5");
        }
        [Test]
        public void TestCalculation2()
        {
            CalculatorClass.CalculateOnConsole("1+2-3+4-5");
        }
    }
}