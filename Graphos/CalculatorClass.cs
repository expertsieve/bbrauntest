﻿/********************************************************************************************************************
 * COMPANY  : B BRAUN
 * SUBJECT  : TEST
 * CLASS    : CALCULATOR CLASS
 * DATE     : MAY 13TH 2020
 * AUTHOR   : OSCAR ROLANDO ALVAREZ
 * EMAIL    : oralvarez@gmail.com
 * *****************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Graphos
{
    class Element {
        int position;
        int type;
        string value;

        public Element(int position, int type, string value) {
            this.position = position;
            this.type = type;
            this.value = value;
        }
    }

    public static class CalculatorClass
    {
        public static void CalculateOnConsole(string value)
        {
            // 001. ---- SPLIT NUMBERS AND SYMBOLS

            int number = 0;

            char[] splitChar = { '+', '-', '*', '/' };
            char[] splitNumber = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

            string[] theValues = value.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            string[] theOperators = value.Split(splitNumber, StringSplitOptions.RemoveEmptyEntries);

            List<Int32> values = new List<Int32>();
            List<String> operators = new List<String>();

            for (int i = 0; i < theValues.Length; i++)
            {
                values.Add(Int32.Parse(theValues[i]));
            }
            for (int i = 0; i < theOperators.Length; i++)
            {
                operators.Add(theOperators[i]);
            }

            int z = 0;
            int counter = 1;

            // 002. ---- PREPARING CALCULATION ---
            z = 0;
            for (int x = 0; x<values.Count; x++)
            {
                if (x == 0)
                {
                    number = values[x];
                }
                else
                {
                    counter = 0;
                    do
                    {
                        string s = operators[z];
                        number = Calculate(number, values[x], s);
                        z++;
                        counter = 1;
                    }
                    while (counter == 0);
                }
            }

            System.Console.WriteLine(string.Format("Calculation result: {0}", number));

        }

        // 003. ---- ROUTINE FOR CALCULATIONS ---
        private static int Calculate(int number1, int number2, string mathOperator)
        {
            int value = 0;

            switch (mathOperator) { 
                case "+" : value = number1 + number2; break;
                case "-" : value = number1 - number2; break;
                case "*" : value = number1 * number2; break;
                case "/" : value = number1 / number2; break;
            }

            return value;
        }
    }
}
